# WarmUpProject Frontend

# Interface
![demo.png](./demo.png)

# Resume
This React project displays news that comes from the backend and has a title, a URL, and is not in a deleted state.
This project used the Styled-Component library to create components directly with static or customizable styles.

# Features

## Banner
For the banner, a component was created along with its respective style.
This component contains an h1 tag with the text: "HN Feed" and an h2 tag with the text: "we 💚 hackernews".

## News

This section shows all the news obtained from the backend.
For this list, a new component was created, which passes the obtained data to a specific component for each row.
The row is made up of four components: the news title, the author, the date, and a button component that allows us to delete a news.

## Available Scripts
In the forntend directory, you can run:

```
npm start
```

Runs the app in the development mode.
Open http://localhost:3000 to view it in your browser.



