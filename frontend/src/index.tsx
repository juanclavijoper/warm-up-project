import React from 'react';
import ReactDOM from 'react-dom/client';
import { ThemeProvider } from 'styled-components';
import App from './App';
import reportWebVitals from './reportWebVitals';
import defaultStyle from './components/GlobalTheme/defaultStyle';
import GlobalStyle from './components/GlobalTheme/globalStyle';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <React.StrictMode>
    <ThemeProvider theme={defaultStyle}>
      <GlobalStyle />
      <App />
    </ThemeProvider>
  </React.StrictMode>
);
reportWebVitals();
