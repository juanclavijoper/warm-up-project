import StyledSubtitleBanner from "./SubtitleBannerStyle/subtitleBanner.styled";

export default function SubtitleBanner({ children }: { children: React.ReactNode; }) {
    return <StyledSubtitleBanner>{children}</StyledSubtitleBanner>;
  }

