import React from 'react';
import StyledCardText from './StyleCardText/cardText.styled';

export default function CardText({ children, type }: { children: React.ReactNode; type: string; }) {
  return <StyledCardText type={type}>{children}</StyledCardText>;
}
