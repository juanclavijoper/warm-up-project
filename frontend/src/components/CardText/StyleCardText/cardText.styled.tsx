import styled from 'styled-components';

interface TypeText {
  readonly type: string;
}

const StyledCardText = styled.td<TypeText>`
  margin-top: 20px;
  color: ${(props) => 
    props.type === 'author' ? 
    props.theme.Author.fontColor : props.theme.Title_Time.fontColor};
  padding-left: 30px;
`;

export default StyledCardText;
