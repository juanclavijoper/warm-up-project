import { DefaultTheme } from 'styled-components';

const defaultStyle: DefaultTheme = {
  
  Row: {
    bgColor: '#fff',
    border: '1px #ccc',
    hoverBgColor: '#D8D8D8', //PREGUNTAR SI PUEDO DEJHAR ESTE COLOR EN VEZ DEL fafafa que se nota muy poco :c
    fontSize: '13pt',
  },

  Title_Time: {
    fontColor: '#333',
    fontSize: '13pt',
  },

  Author: {
    fontColor: '#999',
  },

};

export default defaultStyle;

