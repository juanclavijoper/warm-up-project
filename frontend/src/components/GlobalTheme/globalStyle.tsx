import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  *{
    font-family: Comic Sans MS
    
  }

  body {
    margin: 0;
  }
  
  h1{
    margin: 0;
    font-size: 70px;
  }

  h2{
    margin: 0;
  }

  a {
    text-decoration: none;
  }
`;

export default GlobalStyle;
