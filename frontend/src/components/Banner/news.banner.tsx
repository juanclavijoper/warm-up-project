import SubtitleBanner from '../SubtitleBanner/subtitleBanner';
import TitleBanner from '../TitleBanner/titleBanner';
import StyledBanner from './BannerStyle/banner.styled';

export default function Banner() {
  return (
    <StyledBanner>
      <TitleBanner>HN Feed</TitleBanner>
      <SubtitleBanner> We 💚 Hacker News!</SubtitleBanner>
    </StyledBanner>
  );
}
