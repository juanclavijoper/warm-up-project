import styled from 'styled-components';

const StyledBanner = styled.div`
  color: #fff;
  background-color: ${(props) => 
    props.theme.Title_Time.fontColor
  };
  padding: 50px;
`;

export default StyledBanner;
