import StyledSection from './RowSectionStyle/rowSection.styled';

const RowSection = ({ children }: { children: React.ReactNode }) => {
  return (
    <StyledSection >
      {children}
    </StyledSection>
  );
};

export default RowSection;
