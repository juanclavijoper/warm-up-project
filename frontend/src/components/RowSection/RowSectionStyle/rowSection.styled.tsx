import styled from 'styled-components';

const StyledSection = styled.div`
  display: flex;
  justify-content: space-between;
  width: 20%;
  margin: 20px 0;
`;
export default StyledSection;