import styled from 'styled-components';
import StyledButton from '../../DeleteButton/DeleteButtonStyle/button.styled';

export const StyledRow = styled.div`
  background-color: ${({ theme }) => theme.Row.bgColor};
  border: ${({ theme }) => theme.Row.border};
  text-color: #fff;
  font-size: ${({ theme }) => theme.Row.fontSize};
  display: flex;
  cursor: pointer;
  align-items: center;
  justify-content: space-between;
  border-bottom: 2px solid ${({ theme }) => theme.Author.fontColor};
  &:hover {
    background-color: ${({ theme }) => theme.Row.hoverBgColor}; 
    }
  ${StyledButton} {
    visibility: hidden;
    }
  &:hover {
  ${StyledButton} {
    visibility: visible;
    }
  }
`;

export default StyledRow;
