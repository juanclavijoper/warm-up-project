import React from 'react';
import { StyledRow } from './RowStyle/row.styled';

export default function Row({ children }: { children: React.ReactNode }) {
  return <StyledRow>{children}</StyledRow>;
}
