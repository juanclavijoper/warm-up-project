import styled from 'styled-components';
import { TrashCan } from '@styled-icons/fa-regular/TrashCan'

const StyledIcon = styled(TrashCan)`

width: 20px;
cursor: pointer;
text-align: right;
margin-right: 0px;
color: black; 
padding-left: 50px;
padding-right: 50px;
padding-top: 20px;
padding-bottom: 20px;

}
`;

export default StyledIcon;
