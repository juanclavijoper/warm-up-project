import styled from 'styled-components';

const StyledButton = styled.button`
text-align: right;
padding: 0;
border: none;
background: none;
`;

export default StyledButton;
