import StyledTitleBanner from "./TitleBannerStyle/titleBanner.styled";


export default function TitleBanner({ children }: { children: React.ReactNode; }) {
    return <StyledTitleBanner>{children}</StyledTitleBanner>;
}

