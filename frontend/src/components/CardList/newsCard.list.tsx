import { useEffect, useState } from 'react';
import { News } from '../../interfaces/news.interface';
import Row from '../Row/row'
import CardText from '../CardText/cardText';
import { useHandleDelete, useHandleGetNews } from '../../hooks/useHandleNews';
import Button from '../DeleteButton/delete.button';

import StyledIcon from '../DeleteButton/DeleteButtonIcon/icon.styled';
import Section from '../RowSection/rowSection';
import StyledList from './CardListStyle/cardList.styled';
export default function List() {
  const [news, setNews] = useState<News[]>([]);

  const Delete = useHandleDelete;
  const handleNews = useHandleGetNews;

  const filterNews = (news: News) => {
    if (news.deleted_at == null &&
      (news.story_url || news.url)
      && (news.story_title || news.title)
    ) {
      return true;
    }
    return false;
  }

  useEffect(() => {
    const fetchApi = async () => {
      const data = await handleNews();
      setNews(data.filter(filterNews));
    };
    fetchApi();
  });

  return (
    <StyledList>
      {news.map((news: News) => (

          <Row key={news.objectID} >

            <a
              href={news.story_url}
              target="_blank"
              rel="noopener noreferrer">
              <CardText type="">{news.story_title || news.title}</CardText>
              <CardText type="author">-{news.author}-</CardText>
            </a>

            <Section >
              <CardText type="createdAt">{news.created_at}</CardText>
              <Button  onClick= {() => Delete(news.objectID)}>
                <StyledIcon />
              </Button>
            </Section>

          </Row>

      ))}
    </StyledList>
  );
};