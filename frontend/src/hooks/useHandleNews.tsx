import { deleteSingleNew, getNews } from '../service/news.service';
import { News } from '../interfaces/news.interface';
import useFormatDate from './useHandleDate';

export const useHandleGetNews = async () => {
  const data = await getNews();
  const handleFormatDate = useFormatDate;

  const formatedNews  = data
    .sort(
      (a, b) => new Date(b.created_at).getTime() - new Date(a.created_at).getTime()
    )
    .map((news: News) => ({
      ...news,
      created_at: handleFormatDate(new Date(news.created_at)),
    }));
  return formatedNews ;
};

export const useHandleDelete = async (objectID: number) => {
  const res = await deleteSingleNew(objectID);
  return res;

};