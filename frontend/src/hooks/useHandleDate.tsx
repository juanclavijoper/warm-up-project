import dayjs from 'dayjs';
import isYesterday from 'dayjs/plugin/isYesterday';

const useFormatDate = (strTime: Date ) => {
  dayjs.extend(isYesterday);
  const date = dayjs(strTime);
  const today = dayjs();

  if (today.format('DD/MM/YY') === date.format('DD/MM/YY')) {
    return date.format('h:mm a');
  } else if (date.isYesterday()) {
    return 'Yesterday';
  }
  return `${date.format('MMM')} ${date.format('DD')}`;
};

export default useFormatDate;
