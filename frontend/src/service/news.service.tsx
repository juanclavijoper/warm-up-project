import { News } from '../interfaces/news.interface'

export const getNews = async () => {
  const res = await fetch('http://localhost:3001/News');
  const data: News[] = await res.json();
  return data;
};

export const deleteSingleNew = async (objectID: number) => {
  const res = await fetch(`${'http://localhost:3001'}/News/${objectID}`, {
    method: 'delete',
  });
  return res.status;
};