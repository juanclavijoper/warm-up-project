import List from './components/CardList/newsCard.list'
import Banner from './components/Banner/news.banner'

function App() {
  return (
    <div>
      <Banner />
      <List />
    </div>
  );
}

export default App;
