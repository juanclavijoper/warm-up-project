import 'styled-components';

declare module 'styled-components' {
  export interface DefaultTheme {
    Row: {
      bgColor: string;
      border: string;
      hoverBgColor: string;
      fontSize: string;
    };
    Title_Time: {
      fontColor: string;
      fontSize: string;
    };
    Author: {
      fontColor: string;
    };
  }
}

