export interface News {

    readonly objectID: number;

    readonly story_title: string;

    readonly title: string;

    readonly author: string;

    readonly created_at: string;

    readonly story_url: string;

    readonly story_id: string;

    readonly url: string;

    readonly deleted_at: Date;
  }
  
  