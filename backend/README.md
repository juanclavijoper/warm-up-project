# WarmUpProject Backend

# Resume

The server is responsible for entering the data into the database and exposing an API to the React client.

Once an hour, the server application connects to this API that displays recently published articles about Nodejs in Hacker News: https://hn.algolia.com/api/v1/search_by_date?query=nodejs.

The server application inserts the API data into a MongoDB database and also defines a REST API to retrieve the data.

## Structure
```
  objectID: Number
  story_title: String
  title: String
  author: String
  created_at: Date
  story_url: String
  url: String
  deleted_at: Date
  status: Boolean
```
## Technologies
 Server component: Typescript + NestJS + MongoDB + Docker

## Endpoints 

- Get All News: Get all news that is in the database.

- Get Single News endpoint: A specific news is obtained through the objectID.

- Create News: A new instance of a news item is created in the database.

- Delete News: The status of a news item changes to false based on the objectID.

- Find All: The HN API is queried for all news data. 
This has been programmed so that every time the server starts and every hour thereafter it runs the news search on the HN API.

# Available Scripts

### Run the project in dev mode
In the backend directory, you can run:
```
npm run start:dev
```
Runs the app in the development mode and you can run the GetSingleNews, GetAllNews and DeleteNews endpoints through swagger.

Open http://localhost:3001/api to view it in your browser.


### Run the project in production mode

In the project directory, you can run:
```
docker-compose up --build
```
You will be able to run the application deployed in production mode through docker.

## Unit Test

In the backend directory, you can run:
```
npm run test
```
To see unit tests.

In the backend directory, you can run:
```
npm run test:cov
```
To see unit tests coverage.
