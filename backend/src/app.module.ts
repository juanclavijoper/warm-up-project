import { Module } from '@nestjs/common';
import { NewsModule } from './news/module/news.module';
import { MongooseModule } from '@nestjs/mongoose';
import { ApiModule } from './api/api.module';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get<string>('MONGODB_URI'),
      }),
      inject: [ConfigService],
    }),
    NewsModule,
    ApiModule,
    ConfigModule.forRoot(),
  ],
})
export class AppModule {}
