import { Controller, Delete, Get, Param, ParseIntPipe } from '@nestjs/common';
import { NewsService } from '../service/news.service';
import { NewsDocument } from '../schema/news.schema';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('News')
@Controller('News')
export class NewsController {
  constructor(private newsService: NewsService) {}

  @Get('/:objectID')
  getSingleNews(
    @Param('objectID', ParseIntPipe) objectID: number,
  ): Promise<any> {
    return this.newsService.getSingleNews(objectID);
  }

  @Get()
  getAllNews(): Promise<Array<NewsDocument>> {
    return this.newsService.getAllNews();
  }

  @Delete(':objectID')
  deleteNewById(@Param('objectID') objectID: string): Promise<NewsDocument> {
    return this.newsService.deleteNews(Number(objectID));
  }
}
