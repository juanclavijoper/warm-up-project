import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type NewsDocument = News & Document;

@Schema()
export class News {
  @Prop()
  objectID: number;

  @Prop()
  story_title: string;

  @Prop()
  title: string;

  @Prop()
  author: string;

  @Prop()
  created_at: Date;

  @Prop()
  story_url: string;

  @Prop()
  story_id: string;

  @Prop()
  url: string;

  @Prop({
    type: Date,
    default: null,
  })
  deleted_at: Date;

  @Prop({
    default: true,
  })
  status: boolean;
}

export const NewsSchema = SchemaFactory.createForClass(News);
