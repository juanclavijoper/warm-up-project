import { Test, TestingModule } from '@nestjs/testing';
import { NewsService } from '../service/news.service';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { connect, Connection, Model } from 'mongoose';
import { News, NewsSchema } from '../schema/news.schema';
import { NewsRepository } from '../repository/news.repository';
import { getModelToken } from '@nestjs/mongoose';
import {
  mockNewsCreate,
  mockNewsData,
} from '../../../__mocks__/news-service.mock';

describe('ApiService', () => {
  let mongod: MongoMemoryServer;
  let mongoConection: Connection;
  let newsModel: Model<News>;

  let service: NewsService;
  let repository: NewsRepository;

  beforeAll(async () => {
    mongod = await MongoMemoryServer.create();
    const uri = mongod.getUri();
    mongoConection = (await connect(uri)).connection;
    newsModel = mongoConection.model(News.name, NewsSchema);

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        NewsService,
        NewsRepository,
        { provide: getModelToken(News.name), useValue: newsModel },
      ],
    }).compile();

    service = module.get<NewsService>(NewsService);
    repository = module.get<NewsRepository>(NewsRepository);
  });

  afterAll(async () => {
    await mongoConection.dropDatabase();
    await mongoConection.close();
    await mongod.stop();
  });

  describe('Get News Service Test', () => {
    it('should get all News', async () => {
      repository.getAll = jest.fn().mockReturnValueOnce(mockNewsData);

      const data = await service.getAllNews();

      expect(data).toBeDefined();
      expect(data).toEqual(mockNewsData);
    });

    it('should fail get all News', async () => {
      repository.getAll = jest.fn().mockImplementation(() => {
        throw new Error('Error test');
      });

      try {
        await service.getAllNews();
      } catch (e) {
        expect(e).toBeDefined();
        expect(e.status).toEqual(500);
      }
    });
  });

  describe('Create News Service Test', () => {
    it('should create a news', async () => {
      repository.create = jest.fn().mockReturnValueOnce(mockNewsCreate);

      const news = await service.createNews(mockNewsData);

      expect(news).toBeDefined();
      expect(news).toEqual(mockNewsCreate);
    });

    it('should fail create a News', async () => {
      repository.create = jest.fn().mockImplementation(() => {
        throw new Error('Error test');
      });

      try {
        await service.createNews(mockNewsData);
      } catch (e) {
        expect(e).toBeDefined();
        expect(e.status).toEqual(500);
      }
    });
  });

  describe('Delete News Service Test', () => {
    it('should delete a news', async () => {
      repository.delete = jest.fn().mockReturnValueOnce(mockNewsData);

      const news = await service.deleteNews(mockNewsData.objectID);

      expect(news).toBeDefined();
      expect(news).toEqual(mockNewsData);
    });

    it('should fail delete a News', async () => {
      repository.delete = jest.fn().mockImplementation(() => {
        throw new Error('Error test');
      });

      try {
        await service.deleteNews(mockNewsData.objectID);
      } catch (e) {
        expect(e).toBeDefined();
        expect(e.status).toEqual(500);
      }
    });
  });

  describe('Get a News Service Test', () => {
    it('should get a news', async () => {
      repository.getOne = jest.fn().mockReturnValueOnce(mockNewsData);

      const news = await service.getSingleNews(mockNewsData.objectID);

      expect(news).toBeDefined();
      expect(news).toEqual(mockNewsData);
    });

    it('should fail get a News', async () => {
      repository.getOne = jest.fn().mockImplementation(() => {
        throw new Error('Error test');
      });

      try {
        await service.deleteNews(mockNewsData.objectID);
      } catch (e) {
        expect(e).toBeDefined();
        expect(e.status).toEqual(500);
      }
    });
  });
});
