import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { NewsDocument } from '../schema/news.schema';
import { NewsRepository } from '../repository/news.repository';
import { CreateNewsDto } from '../dto/news.dto';

@Injectable()
export class NewsService {
  constructor(private readonly newsRepository: NewsRepository) {}

  private readonly httpException = new HttpException(
    {
      status: HttpStatus.INTERNAL_SERVER_ERROR,
      error: 'Database conection error',
    },
    HttpStatus.INTERNAL_SERVER_ERROR,
  );

  async getAllNews(): Promise<Array<NewsDocument>> {
    try {
      return await this.newsRepository.getAll();
    } catch (error) {
      Logger.error(error);
      throw this.httpException;
    }
  }

  getSingleNews(objectID: number): Promise<NewsDocument> {
    try {
      return this.newsRepository.getOne({ objectID });
    } catch (error) {
      Logger.error(error);
      throw this.httpException;
    }
  }

  createNews(newsData: CreateNewsDto): Promise<NewsDocument> {
    try {
      return this.newsRepository.create(newsData);
    } catch (error) {
      Logger.error(error);
      throw this.httpException;
    }
  }

  deleteNews(objectID: number): Promise<NewsDocument> {
    try {
      return this.newsRepository.delete({ objectID });
    } catch (error) {
      Logger.error(error);
      throw this.httpException;
    }
  }
}
