import {
  IsBoolean,
  IsDate,
  IsInt,
  IsNotEmpty,
  IsString,
} from 'class-validator';

export class CreateNewsDto {
  @IsInt()
  @IsNotEmpty()
  objectID: number;

  @IsString()
  @IsNotEmpty()
  story_title: string;

  @IsString()
  @IsNotEmpty()
  title: string;

  @IsString()
  @IsNotEmpty()
  author: string;

  @IsDate()
  @IsNotEmpty()
  created_at: Date;

  @IsString()
  @IsNotEmpty()
  story_url: string;

  @IsString()
  @IsNotEmpty()
  story_id: string;

  @IsString()
  @IsNotEmpty()
  url: string;

  @IsBoolean()
  @IsNotEmpty()
  status: boolean;
}
