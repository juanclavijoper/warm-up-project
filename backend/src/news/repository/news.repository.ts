import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateNewsDto } from '../dto/news.dto';
import { News, NewsDocument } from '../schema/news.schema';

@Injectable()
export class NewsRepository {
  constructor(@InjectModel(News.name) private newsModel: Model<NewsDocument>) {}

  getAll(): Promise<Array<NewsDocument>> {
    return this.newsModel.find().exec();
  }

  getOne(filters: object): Promise<NewsDocument> {
    return this.newsModel.findOne(filters).exec();
  }

  create(newsData: CreateNewsDto): Promise<NewsDocument> {
    return this.newsModel.create(newsData);
  }

  delete(filters: object): Promise<NewsDocument> {
    return this.newsModel
      .findOneAndUpdate(
        filters,
        { status: false, deleted_at: new Date() },
        { new: true },
      )
      .exec();
  }
}
