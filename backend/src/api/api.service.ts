import { HttpService } from '@nestjs/axios';
import { NewsService } from '../news/service/news.service';
import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { firstValueFrom, Observable } from 'rxjs';
import { CreateNewsDto } from '../news/dto/news.dto';
import { AxiosResponse } from 'axios';

@Injectable()
export class ApiService {
  constructor(
    private readonly httpService: HttpService,
    private readonly newsService: NewsService,
  ) {}

  findAll(): Observable<AxiosResponse<Array<object>>> {
    return this.httpService.get(process.env.API_URL);
  }

  @Cron(CronExpression.EVERY_HOUR)
  async handleApiData() {
    Logger.debug('cron job');
    const res: any = await firstValueFrom(this.findAll());
    const news: Array<object> = res.data.hits;
    news.map(async (data: CreateNewsDto) => {
      const news = await this.newsService.getSingleNews(data.objectID);
      if (!news) {
        this.newsService.createNews(data);
        Logger.debug(`New News ${data.objectID} created`);
      }
    });
  }
}
