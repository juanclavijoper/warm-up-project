import { Module } from '@nestjs/common';
import { NewsModule } from 'src/news/module/news.module';
import { HttpModule } from '@nestjs/axios';
import { ApiService } from './api.service';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [HttpModule, NewsModule, ScheduleModule.forRoot()],
  providers: [ApiService],
})
export class ApiModule {}
