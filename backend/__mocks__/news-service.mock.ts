export const mockNewsCreate = {
  _id: '633770594fa036d121e84b04',
  objectID: 12345678,
  story_title: 'test',
  title: null,
  author: 'test',
  created_at: new Date('2022-09-27T16:30:27.000Z'),
  story_url: 'null',
  story_id: '12345678',
  url: null,
  deleted_at: null,
  status: true,
  __v: 0,
};

export const mockNewsData = {
  objectID: 12345678,
  story_title: 'test',
  title: null,
  author: 'test',
  created_at: new Date('2022-09-27T16:30:27.000Z'),
  story_url: 'null',
  story_id: '12345678',
  url: null,
  deleted_at: null,
  status: true,
};
